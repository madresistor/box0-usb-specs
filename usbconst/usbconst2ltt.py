#!/bin/python

#
# This file is part of box0-usb-specs.
# Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# usbconst2ltt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# usbconst2ltt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with usbconst2ltt.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Compiler for: USB Constant To Latex Table
./usbconst2ltt <input-file> [<output-file>]

June 10 2014
Kuldeep Singh Dhaka
kuldeep@madresistor.com

Licence: GPLv3 or later (with special exception same as GNU Bison)

________________________________________________________________________
Language:

title <table-title>
col coloumn name for table (optional, default:"Name")
[space]<name> <value> <info>
		<name> = name of enum
		<value> = value of enum
		<info> = information about enum
note <note for the whole table>
blankline seperate between tables
"""

import sys
import re

def specs_parse(fname, debug=False):
	with open(fname) as fp:
		coll = list()
		coll_child = list()
		coll_child_mem = list()
		prop = re.compile(r"(.*?) (.*)")
		mem = re.compile(r" (.*?) (.*?) (.*)")

		for line in fp:
			line = line.rstrip()
			if debug:
				print(line)

			if not len(line):
				if debug:
					print("doing seperation")
				if len(coll_child) or len(coll_child_mem):
					dict_res = {'head': 'members', 'data': coll_child_mem}
					coll_child.append(dict_res)
					coll.append(coll_child)
				coll_child = []
				coll_child_mem = []
				continue
			elif line[0] == '#':
				if debug:
					print("ignoring comment")
				continue
			elif line[0] != ' ':
				res = prop.split(line)
				dict_res = {'head': res[1], 'data': res[2]}
				coll_child.append(dict_res)
			else:
				res = mem.split(line)
				dict_res = {'name': res[1], 'value': res[2], 'info': res[3]}
				coll_child_mem.append(dict_res)

		if len(coll_child):
			dict_res = {'head': 'members', 'data': coll_child_mem}
			coll_child.append(dict_res)
			coll.append(coll_child)
		return coll

def specs_print(data):
	for child in data:
		for res in child:
			if res.get('head') == 'members':
				for m in res.get('data'):
					print("%s(%s): %s"%(m.get('name'), m.get('value'), m.get('info')))
			else:
				print("%s: %s"%(res.get('head'), res.get('data')))
		print("\n")

ceil_to_symb = re.compile(r'CEIL\((.*?)\)')
floor_to_symb = re.compile(r'FLOOR\((.*?)\)')
equ_to_symb = re.compile(r'EQU\((.*?)\)')
def do_latex_escape(payload):
	global ceil_to_symb, floor_to_symb, equ_to_symb
	payload = payload.replace('_', '\_')
	payload = payload.replace('<', '\\textless ')
	payload = payload.replace('>', '\\textgreater ')
	payload = payload.replace('`newline`', '\\newline ')
	payload = ceil_to_symb.sub(r'$\\lceil \1 \\rceil$', payload)
	payload = floor_to_symb.sub(r'$\\lfloor \1 \\rfloor$', payload)
	payload = equ_to_symb.sub(r'$\1$', payload)
	return payload

def specs_latex(parsed_data, debug=False):
	fout = ""
	for child in parsed_data:
		#skip if dont contain any members
		if not len(child):
			continue

		title = "---"
		col = 'Name'
		notes = []
		members = []

		for res in child:
			head = res.get('head')
			payload = res.get('data')

			#escape payload as per latex
			if head == 'members':
				for i in payload:
					i['info'] = do_latex_escape(i['info'])
					i['name'] = do_latex_escape(i['name'])
			else:
				payload = do_latex_escape(payload)

			if head == 'title':
				title = payload
			elif head == 'col':
				col = payload
			elif head == 'note':
				notes.append(payload)
			elif head == 'members':
				members = payload
			elif debug:
				print("unknow head (%s)" % head)

		fout += "\\begin{table}[H]\n"
		fout += "\\begin{center}\n"
		fout += "\\caption{%s}\n"%title
		fout += "\\begin{tabularx}{\\textwidth}{|l|l|X|}\n"
		fout += "\\hline \\colhead{%s} & \\colhead{Value} & \\colhead{Description} \\\\ \\hline\n"%col
		for m in members:
			fout += "\\hline %s & %s & %s \\\\\n"%(m['name'], m['value'], m['info'])
		fout += "\\hline\n"
		fout += "\\end{tabularx}\n"
		fout += "\\end{center}\n"

		if len(notes):
			fout += "\\begin{itemize}\n"
			for n in notes:
				fout += "\\item[NOTE] %s\n"%n
			fout += "\\end{itemize}\n"

		fout += "\\end{table}\n"
		fout += "\n"
	return fout

if __name__ == "__main__":
	assert (len(sys.argv) > 1)
	data = specs_parse(sys.argv[1])
	#specs_print(data)
	output = specs_latex(data)
	if(len(sys.argv) > 2):
		opf = open(sys.argv[2], 'w+')
		opf.write(output)
	else:
		print(output)
