#!/bin/python

#
# This file is part of box0-usb-specs.
# Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# usbreq2ltt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# usbreq2ltt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with usbreq2ltt.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Compiler for: USB Request To Latex Table
./usbreq2ltt <input-file> [<output-file>]

June 10 2014
Kuldeep Singh Dhaka
kuldeep@madresistor.com

Licence: GPLv3 or later (with special exception same as GNU Bison)

________________________________________________________________________
Language:

'#' in first char is a comment and is ignored like it never existed

title <title of the command>
bmreq h2d <host to device (default), intf = interface (default), vend = vendor (default)>
breq <command name>
wval <wValue value>
    use ",," to seperate wValue into 2 part and print them into
     HIGH8(split[0]) LOW(split[1])
wind <Windex field>
    default:"HIGH8(<Module ID>) LOW8(<Module Index>)"
wlen <WLength field>
    default "0"
data <Data stage>
    default "None"
stall <stall condition (can be multple)>
note <any other info>

newline mark end of block

"""

import sys
import re

def specs_parse(fname, debug=False):
	with open(fname) as fp:
		coll = list()
		coll_child = list()
		prop = re.compile(r"(.*?) (.*)")

		for line in fp:
			line = line.rstrip()
			if not len(line):
				if debug:
					print("doing seperation")
				if len(coll_child):
					coll.append(coll_child)
				coll_child = []
				continue
			elif line[0] == '#':
				if debug:
					print("ignoring comment")
				continue

			if debug:
				print(line)
			res = prop.split(line)
			dict_res = {'head': res[1], 'data': res[2]}
			coll_child.append(dict_res)

		if len(coll_child):
			coll.append(coll_child)
		return coll

def specs_print(data):
	for child in data:
		for res in child:
			print("%s: %s" % (res.get('head'), res.get('data')))
		print("\n")

def specs_latex(parsed_data, debug=False):
	#fout = "\\usepackage{tabularx}\n"
	fout = ""
	# todo: one side effect of ".*" zero or more is that
	# if multiple ")" is there, it will greedly eat up all until the last one
	ceil_to_symb = re.compile(r'CEIL\((.*)\)')
	floor_to_symb = re.compile(r'FLOOR\((.*)\)')
	equ_to_symb = re.compile(r'EQU\((.*?)\)')
	for child in parsed_data:
		#skip if dont contain any members
		if not len(child):
			continue

		title = "---"
		bmRequestType = {'dir': 'Host $\\rightarrow$ Device', 'recp': 'Interface', 'type': 'Vendor'}
		bRequest = "---"
		wLength = "0x0000"
		wValue = "0x0000"
		wIndex = "HIGH8(bModule) LOW8(bIndex)"
		Data = "None"
		stall = []
		note = []

		for res in child:
			head = res.get('head')
			payload = res.get('data')

			#escape payload as per latex
			payload = payload.replace('_', '\_')
			payload = payload.replace('<', '\\textless ')
			payload = payload.replace('>', '\\textgreater ')
			payload = payload.replace('`newline`', '\\newline ')
			payload = ceil_to_symb.sub(r'$\\lceil \1 \\rceil$', payload)
			payload = floor_to_symb.sub(r'$\\lfloor \1 \\rfloor$', payload)
			payload = equ_to_symb.sub(r'$\1$', payload)

			if head == 'title':
				title = payload
			elif head == 'bmreq':
				for p in payload.split():
					if p == 'h2d':
						bmRequestType['dir'] = 'Host $\\rightarrow$ Device'
					elif p == 'd2h':
						bmRequestType['dir'] = 'Device $\\rightarrow$ Host'
					elif p == 'intf':
						bmRequestType['recp'] = 'Interface'
					elif p == 'dev':
						wIndex = "0x0000"
						bmRequestType['recp'] = 'Device'
					elif p == 'vend':
						bmRequestType['type'] = 'Vendor'
					elif p == 'std':
						bmRequestType['type'] = 'Standard'
					elif debug:
						print("unknown bmRequestType property (%s)" % p)
			elif head == 'breq':
				bRequest = payload
			elif head == 'wval':
				wValue = payload
			elif head == 'wind':
				wIndex = payload
			elif head == 'wlen':
				wLength = payload
			elif head == 'data':
				Data = payload
			elif head == 'stall':
				stall.append(payload)
			elif head == 'note':
				note.append(payload)
			elif debug:
				print("unknow head (%s)" % head)

		#latex header
		fout += "\\begin{table}[H]\n"
		fout += "\\begin{center}\n"
		fout += "\\caption{%s}\n" % (title)
		fout += "\\begin{tabularx}{\\textwidth}{|l|X|}\n"
		fout += "\\hline \\colhead{Name} & \\colhead{Value} \\\\ \\hline \n"
		fout += "\\hline \\colhead{bmRequestType} & %s, %s, %s\\\\\n" % \
			(bmRequestType['dir'], bmRequestType['recp'], bmRequestType['type'])
		fout += "\\hline \\colhead{bRequest} & %s \\\\\n" % (bRequest)


		if len(wValue.split(',,')) > 1:
			wValue = wValue.split(',,', 1)
			wValue = "HIGH8(%s) LOW8(%s)" % (wValue[0], wValue[1])
		else:
			wValue = "%s" % wValue
		fout += "\\hline \\colhead{wValue} & %s \\\\\n" % wValue
		fout += "\\hline \\colhead{wIndex} & %s \\\\\n" % wIndex
		fout += "\\hline \\colhead{wLength} & %s \\\\\n" % (wLength)
		fout += "\\hline \\colhead{Data} & %s \\\\\n" % (Data)
		fout += "\\hline\n"
		fout += "\\end{tabularx}\n"
		fout += "\\end{center}\n"

		#note or stall print at end
		if(len(stall) or len(note)):
			fout += "\\begin{itemize}\n"
			for s in stall:
				fout += "\\item[STALL] %s\n" % s
			for n in note:
				fout += "\\item[NOTE] %s\n" % n
			fout += "\\end{itemize}\n"

		fout += "\\end{table}\n"
		fout += "\n"
	return fout

if __name__ == "__main__":
	assert (len(sys.argv) > 1)
	data = specs_parse(sys.argv[1])
	#specs_print(data)
	output = specs_latex(data)
	if(len(sys.argv) > 2):
		opf = open(sys.argv[2], 'w+')
		opf.write(output)
	else:
		print(output)
