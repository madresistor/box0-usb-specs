Box0 USB Specification
=====================

Box0 protocol on top op USB.
Download: [box0-usb-specs.pdf](box0-usb-specs.pdf)
see box0-v5-firmware for implementation of Device side.
see libbox0 for implementation on Host side (library).

Dependencies
============

* box0-latex-theme (git submodule)

Getting involved
================

get on to #box0 on IRC freenode

Licence
=======

GNU Free Documentation Licence, Version 1.3 or later

Credits and references
======================

https://www.madresistor.com/

Maintainer
==========

Kuldeep Singh Dhaka <kuldeep@madresistor.com>
