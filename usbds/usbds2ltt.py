#!/bin/python

#
# This file is part of box0-usb-specs.
# Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# usbds2ltt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# usbds2ltt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with usbds2ltt.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Compiler for: USB Data Structure To Latex Table
./usbds2ltt <input-file> [<output-file>]

June 10 2014
Kuldeep Singh Dhaka
kuldeep@madresistor.com

Licence: GPLv3 or later (with special exception same as GNU Bison)

________________________________________________________________________
Language:

lines starting with a '#' are comments

bLength automatically generated
bDescriptorType is automatically placed

title <title of table>
name <name of struct>
dt <descriptor-type>
   if <descriptor-type> is provided
   it is assumed that it is a usb descriptor and bDescriptorType = <descriptor-type>
     and bLength is calculated

[space]<name of member> <value-type> <info>		-> single member
[space]<name of member>[<size>] <value-type> <info> ->array member
				if <size> can be NULL
in <name of member> first char:
			b	= 1byte
			w	= 2byte
			l	= 4byte
			<number> = <number> bytes
			  where <number> can be 1,2,4
note <information about struct>

when a newline is received, end of descriptor is done

example:

bValue NUMBER this is used for good for nothing
bValues[] NUMBER this is array is good for nothing
"""

import sys
import re

def specs_parse(fname, debug=False):
	with open(fname) as fp:
		coll = list()
		coll_child = list()
		coll_child_mem = list()
		prop = re.compile(r"(.*?) (.*)")
		mem = re.compile(r" (.*?)(\[.*?\])? (.*?) (.*)")

		for line in fp:
			line = line.rstrip()
			if debug:
				print(line)

			if not len(line):
				if debug:
					print("doing seperation")
				if len(coll_child) or len(coll_child_mem):
					dict_res = {'head': 'members', 'data': coll_child_mem}
					coll_child.append(dict_res)
					coll.append(coll_child)
				coll_child = []
				coll_child_mem = []
				continue
			elif line[0] == '#':
				if debug:
					print("ignoring comment")
				continue
			if line[0] != ' ':
				res = prop.split(line)
				dict_res = {'head': res[1], 'data': res[2]}
				coll_child.append(dict_res)
			else:
				res = mem.split(line)
				arr_len = None if res[2] is None else res[2][1:-1]
				dict_res = {
					'name': res[1],
					'array':arr_len,
					'type': res[3],
					'info': res[4]
				}
				coll_child_mem.append(dict_res)

		if len(coll_child):
			dict_res = {'head': 'members', 'data': coll_child_mem}
			coll_child.append(dict_res)
			coll.append(coll_child)
		return coll

def specs_print(data):
	for child in data:
		for res in child:
			if res.get('head') == 'members':
				for m in res.get('data'):
					if m.get('array') is None:
						print("%s: %s %s"%(m.get('name'), m.get('type'), m.get('info')))
					else:
						print("%s%s: %s %s"%(m.get('name'), m.get('array'), m.get('type'), m.get('info')))
			else:
				print("%s: %s"%(res.get('head'), res.get('data')))
		print("\n")

ceil_to_symb = re.compile(r'CEIL\((.*?)\)')
floor_to_symb = re.compile(r'FLOOR\((.*?)\)')
equ_to_symb = re.compile(r'EQU\((.*?)\)')
def do_latex_escape(payload):
	global ceil_to_symb, floor_to_symb, equ_to_symb
	payload = payload.replace('_', '\_')
	payload = payload.replace('<', '\\textless ')
	payload = payload.replace('>', '\\textgreater ')
	payload = payload.replace('`newline`', '\\newline ')
	payload = ceil_to_symb.sub(r'$\\lceil \1 \\rceil$', payload)
	payload = floor_to_symb.sub(r'$\\lfloor \1 \\rfloor$', payload)
	payload = equ_to_symb.sub(r'$\1$', payload)
	return payload

def specs_latex(parsed_data, debug=False):
	fout = ""
	for child in parsed_data:
		#skip if dont contain any members
		if not len(child):
			continue

		title = "---"
		name = '---'
		notes = []
		members = []
		usb_descriptor_type = None

		for res in child:
			head = res.get('head')
			payload = res.get('data')

			#escape payload as per latex
			if head == 'dt':
				usb_descriptor_type = payload
			if head == 'members':
				for i in payload:
					for n in ('info', 'name', 'type'):
						i[n] = do_latex_escape(i[n])
			else:
				payload = do_latex_escape(payload)

			if head == 'title':
				title = payload
			elif head == 'name':
				name = payload
			elif head == 'note':
				notes.append(payload)
			elif head == 'members':
				members = []
				offset = 0
				if usb_descriptor_type is not None:
					members.append({
						'offset': 0,
						'field': 'bLength',
						'size': 1,
						'value': 'Number',
						'desc': 'Size of this descriptor in bytes'
					})
					members.append({
						'offset': 1,
						'field': 'bDescriptorType',
						'size': 1,
						'value': do_latex_escape(usb_descriptor_type),
						'desc': 'Descriptor type'
					})
					offset = 2

				bLength_add_ch = None
				for m in payload:
					name = m.get('name')
					size_byte = {
						'b': 1,
						'w': 2,
						'd': 4,
						'i': 1, # usb string descriptor index is always 1byte
						'1': 1,
						'2': 2,
						'4': 4,
						'8': 8
					}.get(name[0], "sizeof(%s[0])" % name)

					# use first character as byte-size if nothing is possible
					if name[0] in ('1', '2', '4', '8'):
						name = name[1:]

					#limitation: does not support array filed with
					#  constant value to place all of them
					# also, do not support multiple array (of constant or variable length)
					if m.get('array') is None:
						dict_m = {
							'size': size_byte,
							'value': m.get('type'),
							'desc': m.get('info'),
							'offset': offset,
							'field': name
						}
						members.append(dict_m)
						if isinstance(size_byte, int):
							offset += size_byte
						else:
							offset = str(offset) + " + " + str(size_byte)
					else:
						#head part
						dict_m = {
							'size': size_byte,
							'value': m.get('type'),
							'desc': '{' + m.get('info')+ " \par} Index: 0",
							'offset': offset,
							'field': name + " [0]"
						}
						members.append(dict_m)

						#..... part
						dict_m = {
							'size': '\dots',
							'value': '\dots',
							'desc': '\dots',
							'offset': '\dots',
							'field': '\dots'
						}
						members.append(dict_m)

						if len(m.get('array')):
							bLength_add_ch = m.get('array')
						else:
							bLength_add_ch = "N"

						#end end part
						index_offset = "%s - 1" % bLength_add_ch
						byte_offset = index_offset
						if isinstance(size_byte, str):
							byte_offset = "((%s) * %s)" % (byte_offset, size_byte)
						elif isinstance(size_byte, int) and size_byte > 1:
							byte_offset = "((%s) * %i)" % (byte_offset, size_byte)


						byte_offset = "%i + %s" % (offset, byte_offset)
						dict_m = {
							'size': size_byte,
							'value': m.get('type'),
							'desc': '{' + m.get('info') + ("\\par} Index: %s" % index_offset),
							'offset': byte_offset,
							'field': name + ("[%s]" % index_offset)
						}
						members.append(dict_m)
				if usb_descriptor_type is not None:
					if bLength_add_ch is None:
						value = str(offset)
					elif isinstance(bLength_add_ch, int):
						#if we can calculate the size in bytes
						value = bLength_add_ch
						if isinstance(size_byte, str):
							value = "%s + (%s * %s)" % (str(offset), str(value), size_byte)
						elif isinstance(size_byte, int) and size_byte > 1:
							value = value * size_byte
							value = "%s + %s" % (str(offset), str(value))
						else:
							value = str(offset + value)
					else:
						#size cannot be calculate in bytes
						# assume them as string
						value = bLength_add_ch
						if isinstance(size_byte, str) or \
							(isinstance(size_byte, int) and size_byte > 1):
							value = "(%s * %s)" % (value, str(size_byte))
						if offset > 0:
							value = "%i + %s" % (offset, value)

					members[0]['value'] = '$' + value + '$'

			elif debug:
				print("unknow head (%s)" % head)

		fout += "\\begin{table}[H]\n"
		fout += "\\begin{center}\n"
		fout += "\\caption{%s}\n"%title

		# Ref: http://tex.stackexchange.com/a/4712/90687
		tabular_ = ">{\\begin{varwidth}{%f\\linewidth}}l<{\\end{varwidth}}"
		tabular_offset = tabular_ % 0.2
		tabular_field = tabular_ % .15
		tabular_size = tabular_ % .2
		tabular_value = "l"
		tabular_desc = "X"
		fout += "\\begin{tabularx}{\\textwidth}{|%s|%s|%s|%s|%s|}\n" % \
			(tabular_offset, tabular_field, tabular_size, tabular_value, tabular_desc)
		fout += "\\hline \\colhead{Offset} & \\colhead{Field} & \\colhead{Size} & \\colhead{Value} & \\colhead{Description} \\\\ \\hline \n"
		for i in members:
			fout += "\\hline $%s$ & %s & $%s$ & %s & %s \\\\\n"%(i.get('offset'), i.get('field'), i.get('size'), i.get('value'), i.get('desc'))
		fout += "\\hline\n"
		fout += "\\end{tabularx}\n"
		fout += "\\end{center}\n"

		if len(notes):
			fout += "\\begin{itemize}\n"
			for n in notes:
				fout += "\\item[NOTE] %s\n"%n
			fout += "\\end{itemize}\n"

		fout += "\\end{table}\n"
		fout += "\n"
	return fout

if __name__ == "__main__":
	assert (len(sys.argv) > 1)
	data = specs_parse(sys.argv[1])
	#specs_print(data)
	output = specs_latex(data)
	if(len(sys.argv) > 2):
		opf = open(sys.argv[2], 'w+')
		opf.write(output)
	else:
		print(output)
