PDFLATEX=pdflatex

all:
	$(MAKE) -C usbconst all
	$(MAKE) -C usbds all
	$(MAKE) -C usbreq all
	$(PDFLATEX) -interaction=nonstopmode box0-usb-specs.tex

clean:
	-rm {,extra/}*.aux {,/extra}*.lof *.toc *.lot *.log *.out
	$(MAKE) -C usbconst clean
	$(MAKE) -C usbds clean
	$(MAKE) -C usbreq clean
